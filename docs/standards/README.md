# API M Standards

The following standards must be adhered to. When in doubt, please reach out for clarification.

* [Audio sampling rate and packet lengths](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/audio_sampling_rates_packet_lengths.md)
* [Authentication](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/authentication.md)
* [Keeping Time](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/keeping_time.md)
* [Standard Sensor Units](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/standard_sensor_units.md)
* [Storing Audio Data](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/storing_audio_data.md)
* [Storing Single Channel Data (Barometer, Light, Proximity)](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/storing_single_channel_data.md)
* [Storing XYZ Channel Data (Gyroscope, Accelerometer, Magnetometer)](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/storing_xyz_channel_data.md)
* [Storing Location Data](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/storing_location_data.md)
* [Storing Image Data](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/storing_image_data.md)
* [Collecting station metrics](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/station_metrics.md)
* [Compression/Decompression](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/compression.md)
* [Filenames and Directory Layout](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/filenames_and_directory_structures.md)
* [Command, Control, and Communication](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/c3po.md)
* [Logging](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/logging.md)
* [Client/Server Communications](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/client_server_comms.md)
* [E2E Encrypting](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/e2e_encryption.md)
* [User Tiers](https://bitbucket.org/redvoxhi/redvox-api-1000/src/master/docs/standards/user_tiers.md)








